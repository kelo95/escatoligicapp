import { firebase } from 'src/plugins/firebase'
import Modal from 'components/modal.vue'
// import menu from 'src/router/menu.json'
import menu from 'src/router/routes.js'
import moment from 'moment'
import Vue from 'vue'
import { Quasar } from 'quasar'

export const commonMixins = {
  components: { Modal },
  data () {
    return {
      menu: menu.filter(m => m.isVisible)
    }
  },
  methods: {
    changeI18n: function (locale, $route, updateParams = true) {
      // $i18n.locale = locale
      locale = locale || navigator.language || navigator.userLanguage
      // locale = locale.split('-')[0]
      switch (locale.split('-')[0]) {
        case 'es':
          locale = 'es'
          break
        case 'en':
          locale = 'en'
          break
        default:
          locale = 'es'
      }

      if (!Vue.i18n.localeExists(locale)) {
        locale = Vue.i18n.locale()
        updateParams = true
      }

      Vue.i18n.set(locale)
      moment.locale(locale)
      firebase.auth().languageCode = locale

      if (updateParams) $route.replace({ params: { lang: locale } })
      locale = locale === 'en' ? 'en-uk' : locale
      // dynamic import, so loading on demand only
      import(`quasar-framework/i18n/${locale}`).then(lang => {
        Quasar.i18n.set(lang.default)
      })
    },
    sleep: function (milliseconds) {
      var start = new Date().getTime()
      for (var i = 0; i < 1e7; i++) {
        if (new Date().getTime() - start > milliseconds) {
          break
        }
      }
    }
  },
  filters: {}
}

export const momentMixins = {
  components: { Modal },
  data () {
    return {
      menu: menu,
      TODAY: moment()
        .clone()
        .startOf('day'),
      YESTERDAY: moment()
        .clone()
        .subtract(1, 'days')
        .startOf('day'),
      A_WEEK_OLD: moment()
        .clone()
        .subtract(7, 'days')
        .startOf('day')
    }
  },
  methods: {
    getWeek (date) {
      return moment(date).week()
    },
    dateBetween (date, startDate, endDate) {
      return (
        (moment(startDate).isBefore(moment(date)) &&
          moment(endDate).isAfter(moment(date))) ||
        moment(startDate).isSame(moment(date), 'day') ||
        moment(endDate).isSame(moment(date), 'day')
      )
    },
    format (date) {
      return moment(date)
        .locale(Vue.i18n.locale())
        .format()
    },
    formatDateTime (date, format = 'HH:mm:ss DD/MM/YYYY') {
      return date
        ? moment(date)
          .locale(Vue.i18n.locale())
          .format(format)
        : ''
    },
    formatDate (date) {
      return momentMixins.methods.formatDateTime(date, 'DD/MM/YYYY')
    },
    formatTime (date) {
      return momentMixins.methods.formatDateTime(date, 'HH:mm:ss')
    },
    fromNow (date) {
      return moment(date)
        .locale(Vue.i18n.locale())
        .fromNow()
        .format()
    },
    parseToDateTime (date, fromFormat, toFormat = '') {
      return moment(date, fromFormat)
        .locale(Vue.i18n.locale())
        .format(toFormat)
    },
    asSeconds (time) {
      return moment.duration(momentMixins.methods.formatTime(time)).asSeconds()
    },
    secondsToTime (seconds, toFormat = 'HH:mm:ss') {
      return seconds
        ? moment()
          .startOf('day')
          .seconds(seconds)
          .format(toFormat)
        : ''
    },
    isToday (date) {
      return moment(date).isSame(
        moment()
          .clone()
          .startOf('day'),
        'd'
      )
    },
    isYesterday (date) {
      return moment(date).isSame(
        moment()
          .clone()
          .subtract(1, 'days')
          .startOf('day'),
        'd'
      )
    },
    isWithinAWeek (date) {
      return moment(date).isAfter(
        moment()
          .clone()
          .subtract(7, 'days')
          .startOf('day')
      )
    },
    /* isTwoWeeksOrMore (date) {
      return !this.isWithinAWeek(date)
    }, */
    isWithinAMonth (date) {
      return moment(date).isSame(moment().clone(), 'M')
    },
    isTwoMonthOrMore (date) {
      return !this.isWithinAMonth(date)
    }
  },
  filters: {
    format (date) {
      return momentMixins.methods.format(date)
    },
    formatDateTime (date, format = 'DD/MM/YYYY HH:mm:ss') {
      return momentMixins.methods.formatDateTime(date, format)
    },
    formatDate (date) {
      return momentMixins.methods.formatDateTime(date, 'DD/MM/YYYY')
    },
    formatTime (date) {
      return momentMixins.methods.formatDateTime(date, 'HH:mm:ss')
    },
    fromNow (date) {
      return momentMixins.methods.fromNow(date)
    },
    parseToDateTime (string, fromFormat, toFormat) {
      return momentMixins.methods.parseToDateTime(string, fromFormat, toFormat)
    },
    secondsToTime (seconds, toFormat) {
      return momentMixins.methods.secondsToTime(seconds, toFormat)
    }
  }
}
