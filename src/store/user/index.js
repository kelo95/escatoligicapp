import Vue from 'vue'

const state = { status: '', profile: {} }

const getters = {
  getProfile: state => state.profile,
  isProfileLoaded: state => !!state.profile && !!state.profile.displayName
}

const actions = {
  USER_LOGIN: ({ commit }, user) => {
    let keys = ['displayName', 'email', 'photoURL']

    user = keys.reduce(function (obj, key) {
      if (user.hasOwnProperty(key)) obj[key] = user[key]
      return obj
    }, {})
    commit('USER_LOGIN', user)
  },
  USER_LOGOUT: ({ commit }) => {
    commit('USER_LOGOUT')
  }
}

const mutations = {
  USER_REQUEST: state => {
    state.status = 'loading'
  },
  USER_LOGIN: (state, resp) => {
    state.status = 'success'
    Vue.set(state, 'profile', resp)
  },
  USER_ERROR: state => {
    state.status = 'error'
  },
  USER_LOGOUT: state => {
    state.profile = {}
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
