import { momentMixins } from '@mixins/index.js'
import Vue from 'vue'

const getDatePoop = poop => {
  let dateType = 'previousmonths'
  if (momentMixins.methods.isToday(poop.date)) dateType = 'today'
  else if (momentMixins.methods.isYesterday(poop.date)) dateType = 'yesterday'
  else if (momentMixins.methods.isWithinAWeek(poop.date)) dateType = 'week'
  else if (momentMixins.methods.isWithinAMonth(poop.date)) dateType = 'month'
  return dateType
}

const managementPoops = (poop, state, remove = false) => {
  let index = -1
  let dateType = getDatePoop(poop)
  try {
    // state.poops.today = state.poops.today || {}
    if (!state.poops.today) Vue.set(state.poops, 'today', {})
    if (!state.poops.yesterday) {
      Vue.set(state.poops, 'yesterday', {})
    }
    if (!state.poops.week) Vue.set(state.poops, 'week', {})
    if (!state.poops.month) Vue.set(state.poops, 'month', {})
    if (!state.poops.previousmonths) {
      Vue.set(state.poops, 'previousmonths', {})
    }
    for (var key in state.poops) {
      if (!state.poops[key].list) {
        Vue.set(state.poops[key], 'list', [])
      }
      index = state.poops[key].list.findIndex(p => p.id === poop.id)
      if (index > -1) {
        poop.deleted = false
        if (key === dateType && !remove) {
          // update
          state.poops[key].list[index] = poop
          // state.poops[key].list.splice()
          // Vue.set(state.poops[key].list, index, poop)
          break
        } else {
          // change Group || remove
          state.poops[key].list.splice(index, 1)
          index = -1
        }
      }
      state.poops[key].list.sort(
        (a, b) => Date.parse(b.date) - Date.parse(a.date)
      )
    }
    // add if not updated and !remove
    if (index === -1 && !remove) state.poops[dateType].list.push(poop)

    state.poops[dateType].list.sort(
      (a, b) => Date.parse(b.date) - Date.parse(a.date)
    )
    index = state.poops[dateType].list.findIndex(p => p.id === poop.id)
  } catch (err) {
    console.log('Sever initialization failed: ', err.message)
  }
  return index
}

export { getDatePoop, managementPoops }
