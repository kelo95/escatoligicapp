// import { getDatePoop, managementPoops } from 'src/store/poop/methods.js'
import { EventBus } from 'src/store/event-bus.js'
import { Poops } from 'src/plugins/firebase.js'
import { Notify } from 'quasar'
import Vue from 'vue'

export default {
  setPoop ({ commit }, poop) {
    commit('SET_POOP', Object.assign({}, poop))
  },
  async firstLoad ({ dispatch, commit }) {
    Poops().on('child_changed', function (data) {
      let poop = data.val()
      poop.id = data.key
      if (poop.date) poop.date = new Date(poop.date)
      commit('ADD_UPDATE_POOP', { poop })
    })
    Poops().on('child_removed', function (data) {
      let poop = data.val()
      poop.id = data.key
      commit('REMOVE_POOP', poop)
    })
    await dispatch('loadPoops')
  },
  async loadPoops ({ state, commit }) {
    let timeStamp = new Date().getTime()
    // commit('SYNC_POOPS', timeStamp)
    await new Promise((resolve, reject) => {
      let loading = true
      let timeout = setTimeout(function () {
        if (loading) {
          Notify.create({
            message: Vue.i18n.translate('config.cantconected'),
            timeout: 8000, // in milliseconds; 0 means no timeout
            type: 'negative',
            icon: 'sync_problem',
            position: 'bottom',
            actions: [
              {
                label: 'Ok',
                handler: () => {}
              }
            ]
          })
          commit('LOADING_POOPS', false)
        }
        resolve(false)
      }, 5000)

      Poops()
        .orderByChild('date')
        .once('value')
        .then(
          function (snapshot) {
            loading = false
            clearTimeout(timeout)
            snapshot.forEach(function (data) {
              let poop = data.val()
              poop.id = data.key
              if (poop.date) poop.date = new Date(poop.date)
              commit('ADD_UPDATE_POOP', { poop, timeStamp })
            })
            commit('SYNC_POOPS', timeStamp)
            resolve(true)
          },
          function (error) {
            console.log(error)
            reject(error)
          }
        )
    })
  },
  addPoop ({ commit }, poop) {
    if (Object.prototype.toString.call(poop.date) === '[object Date]') {
      poop.date = poop.date.getTime()
    }
    poop.dateReal = new Date().getTime()
    Poops()
      .push(poop)
      .then(res => {
        if (res.key) {
          let focus = true
          poop.id = res.key
          if (poop.date) poop.date = new Date(poop.date)
          commit('ADD_UPDATE_POOP', { poop, focus })
        }
      })
      .catch(function (error) {
        console.log('ADD_UPDATE_POOPS failed: ' + error.message)
      })
  },
  updatePoop ({ state }, poop) {
    if (Object.prototype.toString.call(poop.date) === '[object Date]') {
      poop.date = poop.date.getTime()
    }
    Poops()
      .child(poop.id)
      .update(poop)
      .then(mensaje => {})
      .catch(function (error) {
        console.log('UPDATE_POOP failed: ' + error.message)
      })
  },
  removePoop ({ state }, poop) {
    Poops()
      .child(poop.id)
      .remove()
      .then(function () {
        // commit('REMOVE_POOP', poop)
        EventBus.$emit('removedPoop', poop)
      })
      .catch(function (error) {
        console.log('Remove failed: ' + error.message)
      })
  },
  favPoop ({ state }, poop) {
    Poops()
      .child(`${poop.id}`)
      .update({ fav: !poop.fav })
      .then(mensaje => {})
  },
  removeAllPoop ({ commit }) {
    commit('REMOVE_ALL_POOP')
  }
}
