// import { getDatePoop, managementPoops } from 'src/store/poop/methods.js'
// import VueScrollTo from 'vue-scrollto'
// import Vue from 'vue'

function findIndexById (poops, id) {
  if (poops && poops.length) {
    return poops.findIndex(x => x.id === id)
  } else {
    return -1
  }
}
export default {
  SET_POOP (state, poop) {
    state.poop = poop
  },
  ADD_UPDATE_POOP (state, { poop, timeStamp = null, focus = false }) {
    // Vue.set(state.poops, poop.id, poop)
    /* if (focus) {
      VueScrollTo.scrollTo(
        `${getDatePoop(poop)}-${managementPoops(poop, state)}`
      )
    } */
    if (
      !state.poops ||
      !state.poops.length ||
      !(state.poops instanceof Array)
    ) {
      state.poops = []
    }

    let id = findIndexById(state.poops, poop.id)
    poop.timeStamp = timeStamp
    if (id >= 0) state.poops[id] = poop
    else state.poops.push(poop)
    state.poops.splice()
  },
  REMOVE_POOP (state, poop) {
    poop.deleted = true
    let id = findIndexById(state.poops, poop.id)
    if (id >= 0) state.poops.splice(id, 1)
  },
  SYNC_POOPS (state, timeStamp) {
    if (state.poops && state.poops.length) {
      state.poops = state.poops
        .filter(function (poop) {
          return poop.timeStamp === timeStamp
        })
        .filter((poop, pos, arr) => {
          return arr.map(mapPoop => mapPoop['id']).indexOf(poop['id']) === pos
        })
    }
  },
  REMOVE_ALL_POOP (state) {
    state.poops = {}
    state.poop = null
  }
}
