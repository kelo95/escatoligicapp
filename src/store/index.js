import createPersistedState from 'vuex-persistedstate'
import { version } from '../plugins/config.js'
import poop from './poop'
import user from './user'
import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)
// const debug = process.env.NODE_ENV !== 'production'
if (window.localStorage.getItem('version') !== version) {
  window.localStorage.clear()
  window.sessionStorage.clear()
  window.localStorage.setItem('version', version)
}
// console.log('Vuex Debug?', debug)
const store = new Vuex.Store({
  strict: false,
  plugins: [createPersistedState()],
  state: {
    isOnline: navigator.onLine || false
  },
  actions: {
    updateOnlineStatus ({ commit }, status) {
      commit('Update_Online_Status', status)
    }
  },
  mutations: {
    Update_Online_Status (state, status) {
      state.isOnline = status
    }
  },
  modules: {
    poop,
    user
  }
})
export default store
/*
import Vue from 'vue'
import Vuex from 'vuex'

import example from './module-example'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    example
  }
})

export default store
*/
