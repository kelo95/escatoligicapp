import { Bar } from 'vue-chartjs'

export default {
  extends: Bar,
  props: ['data', 'options'],
  watch: {
    data: {
      handler: function (val) {
        // this.$data._chart.update()
        this.$data._chart.destroy()
        this.renderChart_()
      },
      deep: true
    }
  },
  mounted () {
    this.renderChart_()
  },
  methods: {
    renderChart_ () {
      // this.chartData is created in the mixin.
      // If you want to pass options please create a local options object
      this.renderChart(this.data, this.options)
      let legend = this.$data._chart.generateLegend()
      if (this.options && this.options.legendIdContainer && legend.length) {
        this.$parent.$emit('legendGenerated', {
          legend: legend,
          idContainer: this.options.legendIdContainer
        })
      }
    }
  }
}
