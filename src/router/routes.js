export default [
  /*

    component: load('layouts/Master/Master'),
   {
    path: '/',
    component: () => import('layouts/default'),
    children: [{ path: '', component: () => import('pages/index') }]
  },
  {
    path: '/home',
    component: () => import('layouts/default'),
    children: [{ path: '', component: () => import('pages/index') }]
  },

  {
    // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }, */
  {
    title: 'Index',
    subtitle: 'SubIndex',
    path: 'index',
    component: 'pages/index',
    icon: 'home',
    isVisible: false,
    children: [
      {
        path: '',
        component: 'pages/index'
      }
    ],
    meta: {
      title: 'Index Page',
      metaTags: [
        {
          name: 'description',
          content: 'The home page of our example app.'
        },
        {
          property: 'og:description',
          content: 'The home page of our example app.'
        }
      ]
    }
  },
  {
    title: 'My Poop App',
    path: '',
    component: 'pages/EscatologicApp/inventory',
    icon: 'list',
    isVisible: true,
    labelMenu: 'invertory',
    meta: {
      title: 'My Poop App'
    },
    children: [
      {
        title: 'New',
        path: 'm',
        component: 'pages/EscatologicApp/new',
        icon: 'list',
        isVisible: false,
        meta: {
          title: 'New'
        }
      }
    ]
  },
  {
    title: 'Chart',
    path: 'chart',
    component: 'pages/EscatologicApp/chart',
    icon: 'pie_chart',
    isVisible: true,
    labelMenu: 'charts',
    meta: {
      title: 'Chart'
    }
  },
  {
    title: 'Favorites',
    path: 'favorites',
    component: 'pages/EscatologicApp/favorites',
    icon: 'favorite',
    isVisible: true,
    labelMenu: 'favorites',
    meta: {
      title: 'Favorites'
    }
  },
  {
    title: '¿Tienes alguna duda?',
    path: 'faq',
    component: 'pages/Faq',
    icon: 'info',
    isVisible: true,
    labelMenu: 'faqs',
    meta: {
      title: '¿Tienes alguna duda?'
    }
  },
  {
    title: 'Login',
    path: 'login',
    component: 'pages/Account/login',
    icon: 'home',
    isVisible: false,
    meta: {
      title: 'Login',
      metaTags: [
        {
          name: 'description',
          content: 'The home page of our example app.'
        },
        {
          property: 'og:description',
          content: 'The home page of our example app.'
        }
      ]
    }
  }
]
