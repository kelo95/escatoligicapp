import { commonMixins } from 'src/mixins/index.js'
// import menu from 'src/router/routes.js'
import VueRouter from 'vue-router'
import { Loading } from 'quasar'
import Vue from 'vue'
import { currentUser } from '../plugins/firebase'
// import menu from 'src/router/menu.json'
// import routes from './routes'
/*
The Full Navigation Resolution Flow

    Navigation triggered.
    Call leave guards in deactivated components.
    Call global beforeEach guards.
    Call beforeRouteUpdate guards in reused components.
    Call beforeEnter in route configs.
    Resolve async route components.
    Call beforeRouteEnter in activated components.
    Call global beforeResolve guards.
    Navigation confirmed.
    Call global afterEach hooks.
    DOM updates triggered.
    Call callbacks passed to next in beforeRouteEnter guards with instantiated instances.

*/

Vue.use(VueRouter)

function load (componentRoute) {
  let component = componentRoute.split(/\/(.+)/)
  switch (component[0]) {
    case 'src':
      return () => import(`src/${component[1]}.vue`)
    case 'pages':
      return () => import(`pages/${component[1]}.vue`)
    case 'layouts':
      return () => import(`layouts/${component[1]}.vue`)
    case 'components':
      return () => import(`components/${component[1]}.vue`)
    default:
      return () => import(`${component[1]}.vue`)
  }
}
var routes = [
  {
    path: '/:lang?',
    meta: {
      requiresAuth: false
      /*, metaTags: [
        {
          name: 'description',
          content: Vue.i18n.translate('metaDescription')
        },
        {
          property: 'og:description',
          content: Vue.i18n.translate('metaDescription')
        }
      ] */
    },
    // create a container component
    component: {
      render (c) {
        return c('router-view')
      }
    },
    children: [
      {
        path: 'login',
        name: 'login',
        component: load('pages/Account/login'),
        beforeEnter: (to, from, next) => {
          // alert('afterEach:' + from.path + '==>' + to.path + ';')
          // Vue.storage.getItem('isPendingAuth')
          Loading.show({
            // message: 'Some message',
            messageColor: 'withe',
            spinnerSize: 80, // in pixels
            spinnerColor: 'primary',
            customClass: 'bg-grey-4'
          })
          setTimeout(function () {
            next()
          }, 2000)
        },
        beforeRouteLeave (to, from, next) {
          // called when the route that renders this component is about to
          // be navigated away from.
          // has access to `this` component instance.
          // alert('beforeRouteLeave:' + from.path + '==>' + to.path + ';')
          next()
        }
      },
      {
        path: '',
        name: 'master',
        component: load('layouts/Master/Master'),
        icon: 'home',
        meta: {
          requiresAuth: true
        },
        children: [
          {
            title: 'My Poop App',
            name: 'inventory',
            path: '',
            component: load('pages/EscatologicApp/inventory'),
            icon: 'list',
            isVisible: true,
            labelMenu: 'invertory',
            meta: {
              title: 'My Poop App'
            },
            children: [
              {
                title: 'New',
                path: 'm',
                component: load('pages/EscatologicApp/new'),
                icon: 'list',
                isVisible: false,
                meta: {
                  title: 'New'
                }
              }
            ]
          },
          {
            title: 'Chart',
            path: 'chart',
            component: load('pages/EscatologicApp/chart'),
            icon: 'pie_chart',
            isVisible: true,
            labelMenu: 'charts',
            meta: {
              title: 'Chart'
            }
          },
          {
            title: 'Favorites',
            path: 'favorites',
            component: load('pages/EscatologicApp/favorites'),
            icon: 'favorite',
            isVisible: true,
            labelMenu: 'favorites',
            meta: {
              title: 'Favorites'
            }
          },
          {
            title: '¿Tienes alguna duda?',
            path: 'faq',
            component: load('pages/Faq'),
            icon: 'info',
            isVisible: true,
            labelMenu: 'faqs',
            meta: {
              title: '¿Tienes alguna duda?'
            }
          },
          {
            title: 'Login',
            path: 'login',
            component: load('pages/Account/login'),
            icon: 'home',
            isVisible: false,
            meta: {
              title: 'Login'
            }
          }
        ]
      }
    ]
  },
  {
    path: '*',
    redirect: '/'
  }
]
/* for (var route of menu) {
  if (
    route.hasOwnProperty('title') &&
    route.hasOwnProperty('path') &&
    route.hasOwnProperty('component')
  ) {
    route.component = load(route.component)
    if (route.hasOwnProperty('children')) {
      for (var children of route.children) {
        if (children.hasOwnProperty('component')) {
          children.component = load(children.component)
        }
        if (!children.path) children.path = route.path
      }
    }
    routes[0].children.push(route)
  }
}
 */
routes[0].children.push({ path: '*', component: load('pages/404') })

const Router = new VueRouter({
  /*
   * NOTE! Change Vue Router mode from quasar.conf.js -> build -> vueRouterMode
   *
   * If you decide to go with "history" mode, please also set "build.publicPath"
   * to something other than an empty string.
   * Example: '/' instead of ''
   */

  // Leave as is and change from quasar.conf.js instead!
  mode: process.env.VUE_ROUTER_MODE,
  base: process.env.VUE_ROUTER_BASE,
  // base: '/:lang',
  scrollBehavior: () => ({ y: 0 }),
  routes
})

Router.afterEach((to, from) => {
  /*  console.log(
    'afterEach:  ' + from.fullPath + '  ==>  ' + to.fullPath,
    to.params
  ) */
  commonMixins.methods.changeI18n(to.params.lang, Router, false)
})

// This callback runs before every route change, including on page load.
Router.beforeEach((to, from, next) => {
  let _currentUser = currentUser()
  let requiresAuth = to.matched.some(r => r.meta.requiresAuth)

  if (to.query && to.query.user === 'AdSense') {
    requiresAuth = false
  }
  /* console.log(
    'beforeEach:  ' + from.fullPath + '  ==>  ' + to.fullPath,
    to.params,
    requiresAuth,
    !!_currentUser
  ) */
  // This goes through the matched routes from last to first, finding the closest route with a title.
  // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
  const nearestWithTitle = to.matched
    .slice()
    .reverse()
    .find(r => r.meta && r.meta.title)

  // Find the nearest route element with meta tags.
  const nearestWithMeta = to.matched
    .slice()
    .reverse()
    .find(r => r.meta && r.meta.metaTags)
  // const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags)

  // If a route with a title was found, set the document (page) title to that value.
  if (nearestWithTitle) document.title = nearestWithTitle.meta.title

  // Remove any stale meta tags from the document using the key attribute we set below.
  Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(
    el => el.parentNode.removeChild(el)
  )

  // Skip rendering meta tags if there are none.
  if (!nearestWithMeta) {
    if (requiresAuth && !_currentUser) {
      return next({ name: 'login' })
    } else return next()
  }

  // Turn the meta tag definitions into actual elements in the head.
  nearestWithMeta.meta.metaTags
    .map(tagDef => {
      const tag = document.createElement('meta')

      Object.keys(tagDef).forEach(key => {
        tag.setAttribute(key, tagDef[key])
      })

      // We use this to track which meta tags we create, so we don't interfere with other ones.
      tag.setAttribute('data-vue-router-controlled', '')

      return tag
    })
    // Add the meta tags to the document head.
    .forEach(tag => document.head.appendChild(tag))

  if (requiresAuth && !_currentUser) {
    return next({ name: 'login' })
  } else return next()
})

export default Router
