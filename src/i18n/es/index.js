// This is just an example,
// so you can safely delete all default props below

export default {
  updateAvailable:
    '¡Nueva actualización disponible! Refresque la página para actualizar y ver los últimos cambios jugosos.',
  updateAvailableMobile:
    '¡Nueva actualización disponible! Salga y abra la aplicación para actualizar y ver los últimos jugosos cambios.',
  failed: 'Action failed',
  success: 'Action was successful',
  home: 'Inicio',
  metaDescription:
    'My Poop App es la aplicación para llevar un registro y control sobre sus cacas.',
  login_sing:
    'Accede con tu cuenta de google o con tu correo. !Registro directo!',
  lang: {
    es: 'Español',
    en: 'Ingles'
  },
  config: {
    offline: 'No hay conexión a Internet',
    cantconected: 'Compruebe su conexión y vuelve a intentarlo'
  },
  names: {
    poop: 'Caca'
  },
  menu: {
    invertory: 'Mis Cacas',
    favorites: 'Favoritas',
    charts: 'Gráficas',
    faqs: '¿Tienes alguna duda?'
  },
  add: 'Añadir',
  edit: 'Editar',
  remove: 'Eliminar',
  save: 'Guardar',
  cancel: 'Cancelar',
  favorite: 'Favorito',
  undo: 'Deshacer',
  type: 'Tipo',
  color: 'Color',
  today: 'Hoy',
  yesterday: 'Ayer',
  week: 'Esta semana',
  lastweek: 'La semana pasada',
  month: 'Este mes',
  previousmonths: 'Meses anteriores',
  reload: 'Refrescar',
  'data-table': {
    columns: 'Columnas',
    allCols: 'Todas las Columnas',
    rows: 'Filas',
    selected: {
      singular: 'item seleccionado.',
      plural: 'items seleccionados.'
    },
    clear: 'Limpiar',
    search: 'Buscar',
    all: 'Todo'
  },
  nfcText: {
    waitingTag: 'Escaneando tarjeta ...',
    tagSerial: 'Numero de serie',
    notAvailable: 'Este dispositivo no es compatible con NFC.',
    showSettings:
      'El NFC esta desactivado, por favor activelo para poder leer las tarjetas',
    history: 'Historial',
    noHistory: 'Vacio'
  },
  poop: {
    removed: 'Caca eliminada correctamente.',
    typeOf: '¿Qué Texturas/Tipos de la caca hay?',
    colorOf: '¿Qué Tonos/Colores de la caca hay?',
    byType: 'Por Tipos',
    byColor: 'Por Colores',
    byDays: 'Por dia',
    type: 'Tipos',
    color: 'Colores',
    types: {
      0: {
        Appearance:
          'Trozos duros separados, como nueces o excrementos de oveja, que pasan con dificultad.',
        Indicates: 'Estreñimiento importante'
      },
      1: {
        Appearance: 'Como una salchicha compuesta de fragmentos.',
        Indicates: 'Ligero estreñimiento'
      },
      2: {
        Appearance: 'Con forma de morcilla con grietas en la superficie.',
        Indicates: 'Normal'
      },
      3: {
        Appearance: 'Como una salchicha; o serpiente, lisa y blanda.',
        Indicates: 'Perfecta'
      },
      4: {
        Appearance:
          'Trozos de masa pastosa con bordes definidos, que son defecados fácilmente.',
        Indicates: 'Falta de fibra'
      },
      5: {
        Appearance:
          'Fragmentos blandos y esponjosos con bordes irregulares y consistencia pastosa.',
        Indicates: 'Ligera diarrea'
      },
      6: {
        Appearance: 'Acuosa, sin pedazos sólidos, totalmente líquida.',
        Indicates: 'Diarrea importante'
      }
    },
    colors: {
      0: {
        Appearance: 'Marrón',
        Indicates:
          'Estás bien. La caca es marrón por la bilis que produce tu hígado.'
      },
      1: {
        Appearance: 'Verde',
        Indicates:
          'Puede que la comida se esté moviendo demasiado rápido por tu intestino. O puede que estés comiendo demasiadas verduras de hojas verdes, o comida con colorante verde.'
      },
      2: {
        Appearance: 'Amarillo',
        Indicates:
          'La caca amarilla, grasienta y maloliente indica exceso de grasa, lo que podría deberse a un trastorno de malabsorción como la celiaquía.'
      },
      3: {
        Appearance: 'Negro',
        Indicates:
          'Podría significar que tienes una hemorragia interna debida a una úlcera o a cáncer. Algunas vitaminas que contienen hierro o subsalicilato de bismuto también pueden causar caca de color negro. Presta atención si es pegajosa y ve al médico si estás preocupado.'
      },
      4: {
        Appearance: 'De color claro, blanco o color arcilla',
        Indicates:
          'Si no es lo que ves normalmente, podría significar una obstrucción del conducto biliar. Tambíen podrían causarlo algunos medicamentos. Ve al médico.'
      },
      5: {
        Appearance: 'Con manchas de sangre o rojo',
        Indicates:
          'La presencia de sangre en tu caca podría ser un síntoma de cáncer. Si ves sangre en tus heces, ve al médico inmediatamente.'
      }
    }
  },
  faq: {
    canHelp: 'Hola, ¿en qué podemos ayudarte?',
    search: 'Buscar respuestas'
  },
  construction: 'En construcción ...'
}
