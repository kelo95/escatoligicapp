// This is just an example,
// so you can safely delete all default props below

export default {
  updateAvailable:
    'New Update available! Refresh the page to update and to see the latest juicy changes.',
  updateAvailableMobile:
    'New Update available! Exit and open the app to update and to see the latest juicy changes.',
  failed: 'Action failed',
  success: 'Action was successful',
  home: 'Home',
  metaDescription:
    'My Poop App that enables the user to keep record and control of his pops.',
  login_sing: 'Sign in with your google account or your email.',
  lang: {
    es: 'Spanish',
    en: 'English'
  },
  config: {
    offline: 'Your device is offline',
    cantconected: 'Check your connection and try again'
  },
  names: {
    poop: 'Poop'
  },
  menu: {
    invertory: 'My Poops',
    favorites: 'Favorites',
    charts: 'Charts',
    faqs: 'Do you have any doubt?'
  },
  add: 'Add',
  edit: 'Edit',
  remove: 'Revove',
  save: 'Save',
  cancel: 'Cancel',
  favorite: 'Favorite',
  undo: 'Undo',
  type: 'Type',
  color: 'Color',
  today: 'Today',
  yesterday: 'Yesterday',
  week: 'This week',
  lastweek: 'Last week',
  month: 'This month',
  previousmonths: 'Previous months',
  reload: 'Reload',
  'data-table': {
    selection: 'multiple',
    columns: 'Columns',
    allCols: 'All Columns',
    rows: 'Rows',
    selected: {
      singular: 'item selected.',
      plural: 'items selected.'
    },
    clear: 'Clear',
    search: 'Search',
    all: 'All'
  },
  nfcText: {
    waitingTag: 'Scan a tag…',
    tagSerial: 'Serial number',
    notAvailable: 'Your mobile is not compatible.',
    showSettings: 'The NFC is disabled, please activate it to read the cards',
    history: 'History',
    noHistory: 'Empty'
  },
  poop: {
    removed: 'Poop eliminated correctly.',
    typeOf: 'Types of Poop',
    colorOf: 'Colors of Poop',
    byType: 'By Types',
    byColor: 'By Colors',
    byDays: 'By Day',
    type: 'Types',
    color: 'Colors',
    types: {
      0: {
        Appearance: 'Separate, hard lumps',
        Indicates: 'Severe constipation'
      },
      1: {
        Appearance: 'Lumpy and sausage-like',
        Indicates: 'Mild constipation'
      },
      2: {
        Appearance: 'A sausage shape with cracks in the surface',
        Indicates: 'Normal'
      },
      3: {
        Appearance: 'Like a smooth, soft sausage or snake',
        Indicates: 'Perfect'
      },
      4: {
        Appearance: 'Soft blobs with clear-cut edges',
        Indicates: 'Lacking fiber'
      },
      5: {
        Appearance: 'Mushy consistency with ragged edges',
        Indicates: 'Mild diarrhea'
      },
      6: {
        Appearance: 'Liquid consistency with no solid pieces',
        Indicates: 'Severe diarrhea'
      }
    },
    colors: {
      0: {
        Appearance: 'Brown',
        Indicates:
          "You're fine. Poop is naturally brown due to the bile produced in your liver."
      },
      1: {
        Appearance: 'Green',
        Indicates:
          'Food may be moving through your large intestine too quickly. Or you could have eaten lots of green leafy veggies, or green food colouring.'
      },
      2: {
        Appearance: 'Yellow',
        Indicates:
          'Greasy, foul-smelling yellow poop indicates excess fat, which could be due to a malabsorption disorder like celiac disease.'
      },
      3: {
        Appearance: 'Black',
        Indicates:
          "It could mean that you're bleeding internally due to ulcer or cancer. Some vitamins containing irion or bismuth subsalicylate could cause black poop too. Pay attention if it's sticky, and see a doc if you're worried."
      },
      4: {
        Appearance: 'Light-coloured, whit, or clay-coloured',
        Indicates:
          "If it's not what you're normally seeing, it could mean a bile duct obstruction. Some meds could cause this too. See a doc."
      },
      5: {
        Appearance: 'Blood-stained or Red',
        Indicates:
          'Blood in your poop could be a symptom of cancer. Always see a doc right away if you find blood in your stool.'
      }
    }
  },
  faq: {
    canHelp: 'Hello, how can we help?',
    search: 'Search for answers'
  },
  construction: 'Under construction ...'
}
