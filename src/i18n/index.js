import es from './es'
import enUk from './en-uk'

export default {
  'es': es,
  'en': enUk
}
