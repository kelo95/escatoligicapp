import VueConfigManager from 'vue-config-manager'

const version = '1.0.6'

export { version }
export default ({ app, Vue }) => {
  Vue.directive('focus', {
    inserted: function (el) {
      el.focus()
    }
  })

  Vue.use(VueConfigManager, {
    defaults: {
      debug: false,
      AppName: 'My Poop App',
      version: version,
      api: {
        base: ''
      },
      newsapi: {
        endpoints: {
          top: 'top-headlines',
          everything: 'everything',
          sources: 'sources'
        },
        country: 'es',
        language: 'es',
        key: '9ed8469fe0404c898ecc5696a0f35c14'
      }
    },
    hosts: {
      localhost: {
        debug: true,
        // version: '- Debugging',
        api: {
          base: 'api.localhost.com'
        },
        new: 'ok'
      },
      '127.0.0.1': {
        debug: true,
        version: version,
        api: {
          base: 'api.127.0.0.1.com'
        },
        new: 'ok 2'
      }
    }
  })
}
