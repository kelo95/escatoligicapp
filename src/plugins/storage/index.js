import CookieStorage from './cookie-storage.js'
import LocalStorage from './local-storage.js'
import MemoryStorage from './memory-storage.js'
import SessionStorage from './session-storage.js'

export default {
  install (Vue, options) {
    const storage = StorageFactory(options)
    // 1. Añadir un método global o una propiedad
    Vue.storage = storage

    // 4. Añadir un método de instancia
    Vue.prototype.$storage = storage
  }
}

function StorageFactory (options) {
  switch (options.storageType) {
    case 'localStorage':
      try {
        window.localStorage.setItem('testKey', 'test')
        window.localStorage.removeItem('testKey')
        return new LocalStorage(options.storageNamespace)
      } catch (e) {
        console.log(e)
      }
      break
    case 'sessionStorage':
      try {
        window.sessionStorage.setItem('testKey', 'test')
        window.sessionStorage.removeItem('testKey')
        return new SessionStorage(options.storageNamespace)
      } catch (e) {
        console.log(e)
      }
      break
    case 'memoryStorage':
      return new MemoryStorage(options.storageNamespace)
    default:
    case 'cookieStorage':
      return new CookieStorage(options.cookieStorage)
  }
}
