// import scrollTo from './scrollTo/index.js'
import scrollTo from 'vue-scrollto'
export default ({ Vue }) => {
  Vue.use(scrollTo)
}
