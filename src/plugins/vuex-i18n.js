// load and register the vuex i18n module
import vuexI18n from 'vuex-i18n'
import i18n from '../i18n/index.js'

export default ({ app, Vue, store }) => {
  Vue.use(vuexI18n.plugin, store)
  // store.dispatch('addLocales', i18n)
  // recorremos los idioma añadidos en store
  for (var locale in i18n) {
    if (!Vue.i18n.localeExists(locale)) {
      Vue.i18n.add(locale, i18n[locale])
    }
  }
  // Set the start locale to use
  Vue.i18n.set('es')
  Vue.i18n.fallback('es')
}
