import VuexAuthStore from './vuex-auth-store'

export default {
  install (Vue, store, axios, storage) {
    const moduleName = 'auth'

    // register the auth module in the vuex store
    // preserveState can be used via configuration if server side rendering is used
    store.registerModule('auth', VuexAuthStore, {
      preserveState: false
    })
    // check if the plugin was correctly initialized
    if (store.state.hasOwnProperty(moduleName) === false) {
      console.error(
        'auth: auth vuex module is not correctly initialized. Please check the module name:',
        moduleName
      )
      Vue.prototype.$login = function (username, password) {
        console.error('auth: auth vuex module is not correctly initialized')
        return null
      }
      Vue.prototype.$logout = function () {
        console.error('auth: auth vuex module is not correctly initialized')
        return null
      }
      return
    }

    store.dispatch(`${moduleName}/AUTH_STORAGE`, storage)

    axios.interceptors.response.use(undefined, function (err) {
      return new Promise(function (resolve, reject) {
        if (err.status === 401 && err.config && !err.config.__isRetryRequest) {
          // if you ever get an unauthorized, logout the user
          store.dispatch(`${moduleName}/AUTH_LOGOUT`)
          // you can also redirect to /login if needed !
        }
        throw err
      })
    })

    Vue.$auth = function (reference, timeout) {
      Vue.axios.interceptors.response.use(undefined, function (err) {
        return new Promise(function (resolve, reject) {
          if (
            err.status === 401 &&
            err.config &&
            !err.config.__isRetryRequest
          ) {
            // if you ever get an unauthorized, logout the user
            store.dispatch(`${moduleName}/AUTH_LOGOUT`)
            // you can also redirect to /login if needed !
          }
          throw err
        })
      })
    }

    Vue.prototype.$login = function (username, password) {
      store
        .dispatch(`${moduleName}/AUTH_REQUEST`, { username, password })
        .then(() => {
          Vue.$router.push({ name: 'inventory' })
        })
    }
    Vue.prototype.$logout = function () {
      store.dispatch('AUTH_LOGOUT').then(() => {
        Vue.$router.replace({ name: 'login' })
      })
    }

    /*
    store.getters[`${moduleName}/storage`].setItem('user-token', 'dsdsad')
    let token = store.getters[`${moduleName}/token`]
    if (token) axios.defaults.headers.common['Authorization'] = token
    */
  }
}
