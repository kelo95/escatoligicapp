// import the vuex module for localization
import VuexAuthStore from './vuex-auth-store'
// import the corresponding plugin for vue
import VuexAuthPlugin from './vuex-auth-plugin'

// export both modules as one file
export default {
  store: VuexAuthStore,
  plugin: VuexAuthPlugin
}
