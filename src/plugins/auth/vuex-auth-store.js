import state from './store/state'
import getters from './store/getters'
import mutations from './store/mutations'
import actions from './store/actions'

// define a simple vuex module
const VuexAuthStore = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}

export default VuexAuthStore
