import StorageFactory from './storage/index.js'

export default ({ app, Vue, store }) => {
  Vue.use(StorageFactory, {
    storageType: 'cookieStorage',
    cookieStorage: {
      domain: getCookieDomainUrl(),
      path: '/',
      secure: false
    }
  })
}

function getCookieDomainUrl () {
  try {
    return window.location.hostname
  } catch (e) {}
  return ''
}
