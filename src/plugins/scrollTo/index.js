Element.prototype.getOffsetTop = function () {
  return (
    this.offsetTop + (this.offsetParent ? this.offsetParent.getOffsetTop() : 0)
  )
}

export default {
  install (Vue, options) {
    // 1. Añadir un método global o una propiedad
    Vue.scrollTo = function (reference, timeout) {
      scrollTo(reference, timeout)
    }

    // 4. Añadir un método de instancia
    Vue.prototype.$scrollTo = function (reference, timeout) {
      // mi lógica
      scrollTo(reference, timeout)
    }

    function scrollTo (reference, timeout = 50) {
      Vue.nextTick(() => {
        let element = document.getElementById(reference)
        if (reference && element) {
          let topOff = element.getOffsetTop() - 20
          //  var top = window.pageYOffset || document.documentElement.scrollTop
          timeout = isNaN(timeout) || timeout < 0 ? 50 : timeout
          setTimeout(function () {
            // document.getElementsByTagName('header')[0].classList.add('q-layout-header-hidden')
            if (topOff >= 200) {
              window.scrollTo({ top: topOff, left: 0, behavior: 'smooth' })
            }
            setTimeout(function () {
              element.firstChild.classList.add('animate-pop')
              setTimeout(function () {
                element.firstChild.classList.remove('animate-pop')
              }, 2000)
            }, 250)
          }, timeout)
        }
      })
    }

    // 2. Añadir un asset global (directiva, filtro, ...)
    /* Vue.directive('mi-directiva', {
      bind (el, binding, vnode, oldVnode) {
        // mi lógica ...
      }
    }) */

    // 3. Inyectar más opciones a un componente
    /* Vue.mixin({
      created: function () {
        // mi lógica ...
      }
    }) */
  }
}
