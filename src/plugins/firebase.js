import firebaseui from 'firebaseui'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import { Loading } from 'quasar'
import Router from '../router'
// import VueFire from 'vuefire'
import Vue from 'vue'

/*
const config = {
    apiKey: "AIzaSyBusbgfEIy-3rcgc8Y6tKOYlHgdlUJwjvo",
    authDomain: "miquel-iesperemaria.firebaseapp.com",
    databaseURL: "https://miquel-iesperemaria.firebaseio.com",
    projectId: "miquel-iesperemaria",
    storageBucket: "miquel-iesperemaria.appspot.com",
    messagingSenderId: "966968138627"
 };
*/

const config = {
  apiKey: 'AIzaSyBUFVxWEiKFUhb0xyDgG8aDDL3g8_0KrLA',
  authDomain: 'mypoop-app.firebaseapp.com',
  databaseURL: 'https://mypoop-app.firebaseio.com',
  projectId: 'mypoop-app',
  storageBucket: 'mypoop-app.appspot.com',
  messagingSenderId: '625775984652'
}

var CLIENT_ID = '625775984652-c17khmuf19dl49tg5debkaebf6l6rfpk.apps.googleusercontent.com'

// if (firebase.apps.length === 0) firebase.initializeApp(config)
// firebase.auth().useDeviceLanguage()
let DB = null

var uiConfig = {
  signInSuccessUrl: '/',
  callbacks: {
    // Called when the user has been successfully signed in.
    signInSuccessWithAuthResult: (authResult, redirectUrl) => {
      if (authResult.user) {
        Router.replace('/')
        // Router.replace({ name: 'inventory' })
        Loading.hide()
      }
      if (authResult.additionalUserInfo) {
      }
      // Do not redirect.
      return false
    },
    uiShown: () => {
      // The widget is rendered.
      // Hide the loader.
      Loading.hide()
    }
    /* signInFailure: function (error) {
      return false
    } */
  },
  // Opens IDP Providers sign-in flow in a popup.
  // signInFlow: 'popup',
  signInOptions: [
    // TODO(developer): Remove the providers you don't need for your app.
    {
      provider: firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      // Required to enable this provider in One-Tap Sign-up.
      authMethod: 'https://accounts.google.com',
      // Required to enable ID token credentials for this provider.
      clientId: CLIENT_ID
    },
    {
      provider: firebase.auth.EmailAuthProvider.PROVIDER_ID,
      // Whether the display name should be displayed in Sign Up page.
      requireDisplayName: true
    }
    /* {
      provider: firebase.auth.FacebookAuthProvider.PROVIDER_ID,
      scopes: ['public_profile', 'email', 'user_likes', 'user_friends']
    },
    firebase.auth.TwitterAuthProvider.PROVIDER_ID,
    firebase.auth.GithubAuthProvider.PROVIDER_ID,
    {
      provider: firebase.auth.PhoneAuthProvider.PROVIDER_ID,
      recaptchaParameters: {
        size: 'normal'
      }
    } */
  ],
  // Terms of service url.
  tosUrl: 'https://www.google.com',
  credentialHelper:
    CLIENT_ID && CLIENT_ID !== 'YOUR_OAUTH_CLIENT_ID'
      ? firebaseui.auth.CredentialHelper.GOOGLE_YOLO
      : firebaseui.auth.CredentialHelper.ACCOUNT_CHOOSER_COM
}

export default () => {}
export { firebase, ui, Poops, config, CLIENT_ID, uiConfig, currentUser }

function ui () {
  let firebaserUi = firebaseui.auth.AuthUI.getInstance()
  if (!firebaserUi) {
    firebaserUi = new firebaseui.auth.AuthUI(firebase.auth())
  }
  return firebaserUi
}
function Poops () {
  if (firebase.apps.length === 0) firebase.initializeApp(config)
  DB = firebase.database()
  let user = Vue.storage ? '/' + Vue.storage.getItem('tok') : ''
  return DB.ref('poops' + user)
}
function currentUser () {
  if (firebase.apps.length === 0) firebase.initializeApp(config)
  let user = Vue.storage ? Vue.storage.getItem('tok') : null
  return firebase.auth().currentUser || user
}
