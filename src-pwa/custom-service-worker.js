/*
 * This file (which will be your service worker)
 * is picked up by the build system if BOTH conditions are met:
 *  - You are building for production
 *  - quasar.conf > pwa > workboxPluginMode is set to "InjectManifest"
 */

// While overkill for this specific sample in which there is only one cache,
// this is one best practice that can be followed in general to keep track of
// multiple caches used by a given service worker, and keep them all versioned.
// It maps a shorthand identifier for a cache to a specific, versioned cache name.

// Note that since global state is discarded in between service worker restarts, these
// variables will be reinitialized each time the service worker handles an event, and you
// should not attempt to change their values inside an event handler. (Treat them as constants.)

// If at any point you want to force pages that use this service worker to start using a fresh
// cache, then increment the CACHE_VERSION value. It will kick off the service worker update
// flow and the old cache(s) will be purged as part of the activate event handler when the
// updated service worker is activated.

const CACHE_VERSION = '18-06-28---00-44'
var cacheName = 'exercise-' + CACHE_VERSION
var dataCacheName = 'exercise-data-' + CACHE_VERSION

console.log(CACHE_VERSION)

/* function openWindow (event) {
  /** ** START notificationOpenWindow ****
  const promiseChain = clients.openWindow(self.location.origin) // eslint-disable-line
  event.waitUntil(promiseChain)
  /** ** END notificationOpenWindow ****
} */

function focusWindow (event) {
  /** ** START notificationFocusWindow ****/
  /** ** START urlToOpen ****/
  const urlToOpen = new URL(self.location.origin, self.location.origin).href
  /** ** END urlToOpen ****/
  /** ** START clientsMatchAll ****/
  const promiseChain = clients // eslint-disable-line
    .matchAll({
      type: 'window',
      includeUncontrolled: true
    })
    /** ** END clientsMatchAll ****/
    /** ** START searchClients ****/
    .then(windowClients => {
      let matchingClient = null

      for (let i = 0; i < windowClients.length; i++) {
        const windowClient = windowClients[i]
        // console.log(windowClient, windowClient.url, urlToOpen)
        // if (windowClient.url === urlToOpen) {
        matchingClient = windowClient
        break
        // }
      }
      if (matchingClient) {
        return matchingClient.focus()
      } else {
        return clients.openWindow(urlToOpen) // eslint-disable-line
      }
    })
  /** ** END searchClients ****/

  event.waitUntil(promiseChain)
  /** ** END notificationFocusWindow ****/
}

self.addEventListener('push', function (event) {
  console.log('Push message received', event)
  if (event.data) {
    switch (event.data.text()) {
      default:
        console.warn('Unsure of how to handle push event: ', event.data)
        break
    }
  }
})

self.addEventListener('notificationclick', function (event) {
  console.log('On notification notificationclick: ', event.notification.tag)
  event.notification.close()
  focusWindow(event)
  /* switch (event.notification.data.action) {
    case 'open-window':
      openWindow(event)
      break
    case 'focus-window':
      focusWindow(event)
      break
    default:
      // NOOP
      break
  } */
})

self.addEventListener('notificationclose', function (event) {
  console.log('On notification close: ', event.notification.tag)
  window.dispatchEvent(new Event('notificationclose'))
})

self.addEventListener('install', function (e) {
  console.log('[ServiceWorker] Install')
  e.waitUntil(
    caches.open(cacheName).then(function (cache) {
      console.log('[ServiceWorker] Caching app shell')
      return cache.addAll(self.__precacheManifest)
    })
  )
})

self.addEventListener('activate', function (e) {
  console.log('[ServiceWorker] Activate')
  e.waitUntil(
    caches.keys().then(function (keyList) {
      return Promise.all(
        keyList.map(function (key) {
          if (key !== cacheName) {
            console.log('[ServiceWorker] Removing old cache', key)
            return caches.delete(key)
          }
        })
      )
    })
  )
})

self.addEventListener('fetch', function (e) {
  // console.log('WORKER FETCH CALLED...')
  e.respondWith(
    fetch(e.request)
      .then(function (response) {
        return caches.open(dataCacheName).then(function (cache) {
          // console.log('SAVING >>> ' + e.request.url)
          cache.put(e.request.url, response.clone())
          return response
        })
      })
      .catch(function () {
        return caches.match(e.request).then(function (response) {
          return response || caches.match('/index.html')
        })
      })
  )
})

// Durante respuestas de la red
/* self.addEventListener('fetch', function (event) {
  event.respondWith(
    caches.open('mysite-dynamic').then(function (cache) {
      return cache.match(event.request).then(function (response) {
        return (
          response ||
          fetch(event.request).then(function (response) {
            cache.put(event.request, response.clone())
            return response
          })
        )
      })
    })
  )
}) */

// Caché después red
/* self.addEventListener('fetch', function (event) {
  event.respondWith(
    caches.open('mysite-dynamic').then(function (cache) {
      return fetch(event.request).then(function (response) {
        cache.put(event.request, response.clone())
        return response
      })
    })
  )
})
*/
// https://www.desarrolloweb.com/articulos/estrategias-offline-pwa.html
/*
self.addEventListener('fetch', function (event) {
  event.respondWith(
    fetch(event.request).catch(function () {
      return caches.match(event.request)
    })
  )
})
*/
/*
var CURRENT_CACHES = {
  prefetch: 'prefetch-cache-v' + CACHE_VERSION
}
self.addEventListener('install', function (event) {
  var now = Date.now()

  var urlsToPrefetch = self.__precacheManifest

  // All of these logging statements should be visible via the "Inspect" interface
  // for the relevant SW accessed via chrome://serviceworker-internals
  // console.log('Handling install event. Resources to prefetch:', urlsToPrefetch)

  event.waitUntil(
    caches
      .open(CURRENT_CACHES.prefetch)
      .then(function (cache) {
        var cachePromises = urlsToPrefetch.map(function (urlToPrefetch) {
          // This constructs a new URL object using the service worker's script location as the base
          // for relative URLs.
          var url = new URL(urlToPrefetch, location.href)
          // Append a cache-bust=TIMESTAMP URL parameter to each URL's query string.
          // This is particularly important when precaching resources that are later used in the
          // fetch handler as responses directly, without consulting the network (i.e. cache-first).
          // If we were to get back a response from the HTTP browser cache for this precaching request
          // then that stale response would be used indefinitely, or at least until the next time
          // the service worker script changes triggering the install flow.
          url.search += (url.search ? '&' : '?') + 'cache-bust=' + now

          // It's very important to use {mode: 'no-cors'} if there is any chance that
          // the resources being fetched are served off of a server that doesn't support
          // CORS (http://en.wikipedia.org/wiki/Cross-origin_resource_sharing).
          // In this example, www.chromium.org doesn't support CORS, and the fetch()
          // would fail if the default mode of 'cors' was used for the fetch() request.
          // The drawback of hardcoding {mode: 'no-cors'} is that the response from all
          // cross-origin hosts will always be opaque
          // (https://slightlyoff.github.io/ServiceWorker/spec/service_worker/index.html#cross-origin-resources)
          // and it is not possible to determine whether an opaque response represents a success or failure
          // (https://github.com/whatwg/fetch/issues/14).
          var request = new Request(url, { mode: 'no-cors' })
          return fetch(request)
            .then(function (response) {
              if (response.status >= 400) {
                throw new Error(
                  'request for ' +
                    urlToPrefetch +
                    ' failed with status ' +
                    response.statusText
                )
              }

              // Use the original URL without the cache-busting parameter as the key for cache.put().
              return cache.put(urlToPrefetch, response)
            })
            .catch(function (error) {
              console.error('Not caching ' + urlToPrefetch + ' due to ' + error)
            })
        })

        return Promise.all(cachePromises).then(function () {
          console.log('Pre-fetching complete.')
        })
      })
      .catch(function (error) {
        console.error('Pre-fetching failed:', error)
      })
  )
})

self.addEventListener('activate', function (event) {
  // Delete all caches that aren't named in CURRENT_CACHES.
  // While there is only one cache in this example, the same logic will handle the case where
  // there are multiple versioned caches.
  var expectedCacheNames = Object.keys(CURRENT_CACHES).map(function (key) {
    return CURRENT_CACHES[key]
  })

  event.waitUntil(
    caches.keys().then(function (cacheNames) {
      return Promise.all(
        cacheNames.map(function (cacheName) {
          if (expectedCacheNames.indexOf(cacheName) === -1) {
            // If this cache name isn't present in the array of "expected" cache names, then delete it.
            // console.log('Deleting out of date cache:', cacheName)
            return caches.delete(cacheName)
          }
        })
      )
    })
  )
})

self.addEventListener('fetch', function (event) {
  // console.log('Handling fetch event for', event.request.url)
  event.respondWith(
    // caches.match() will look for a cache entry in all of the caches available to the service worker.
    // It's an alternative to first opening a specific named cache and then matching on that.
    caches.match(event.request).then(function (response) {
      if (response) {
        // console.log('Found response in cache:', response)
        return response
      }
      // console.log('No response found in cache. About to fetch from network...')
      // event.request will always have the proper mode set ('cors, 'no-cors', etc.) so we don't
      // have to hardcode 'no-cors' like we do when fetch()ing in the install handler.
      return fetch(event.request)
        .then(function (response) {
          // console.log('Response from network is:', response)
          return response
        })
        .catch(function (error) {
          // This catch() will handle exceptions thrown from the fetch() operation.
          // Note that a HTTP error response (e.g. 404) will NOT trigger an exception.
          // It will return a normal response object that has the appropriate error code set.
          console.error('Fetching failed:', error)
          throw error
        })
    })
  )
})
*/
