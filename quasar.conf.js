// Configuration for your app
let path = require('path')
module.exports = function (ctx) {
  return {
    // app plugins (/src/plugins)
    // plugins: ['axios', 'i18n', 'config-manager'],
    plugins: [
      'storage',
      'firebase',
      'axios',
      'vuex-i18n',
      'config',
      'scrollTo'
    ],
    css: ['app.styl'],
    extras: [
      ctx.theme.mat ? 'roboto-font' : null,
      'material-icons'
      // 'ionicons',
      // 'mdi',
      // 'fontawesome'
    ],
    supportIE: false,
    build: {
      scopeHoisting: true,
      // vueRouterMode: 'hash',
      vueRouterMode: 'history',
      // vueRouterBase: '/dds',
      // publicPath: '/dds',
      // gzip: true,
      // analyze: true,
      // extractCSS: false,
      // useNotifier: false,
      extendWebpack (cfg) {
        cfg.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules|quasar)/
        })
        cfg.resolve.alias = {
          ...cfg.resolve.alias,
          '@statics': path.resolve(__dirname, './src/statics'),
          '@mixins': path.resolve(__dirname, './src/mixins')
        }
      }
    },
    devServer: {
      // https: true,
      // port: 8080,
      open: true // opens browser wsindow automatically
    },
    // framework: 'all' --- includes everything; for dev only!
    framework: {
      components: [
        'QLayout',
        'QLayoutHeader',
        'QLayoutDrawer',
        'QPageContainer',
        'QPage',
        'QToolbar',
        'QToolbarTitle',
        'QBtn',
        'QIcon',
        'QList',
        'QListHeader',
        'QItem',
        'QItemMain',
        'QItemSide',
        'QFab',
        'QFabAction',
        'QCard',
        'QCardTitle',
        'QCardMain'
      ],
      directives: ['Ripple', 'CloseOverlay'],
      // Quasar plugins
      plugins: ['Notify', 'AddressbarColor', 'Loading', 'Meta'],
      i18n: 'es'
    },
    // animations: 'all' --- includes all animations
    animations: 'all',
    pwa: {
      // cacheExt:'js,html,css,ttf,eot,otf,woff,woff2,json,svg,gif,jpg,jpeg,png,wav,ogg,webm,flac,aac,mp4,mp3',
      workboxPluginMode: 'InjectManifest',
      // workboxOptions: {},
      // cacheExt: 'js,html,css,ttf,eot,otf,woff,woff2,json,svg,gif,jpg,jpeg,png,wav,ogg,webm,flac,aac,mp4,mp3',
      manifest: {
        name: 'My Poop App',
        short_name: 'My Poop App',
        description: 'My Poop App by KeluDev',
        start_url: '/?version=pwa',
        display: 'standalone',
        orientation: 'portrait-primary',
        background_color: '#074b90',
        theme_color: '#0167B1',
        prefer_related_applications: false,
        scope: '/',
        lang: 'es-es',
        icons: [
          /* {
            src: 'statics/icons/icon-128x128.png',
            sizes: '128x128',
            type: 'image/png'
          }, */
          {
            src: 'statics/icons/apple-touch-icon.png',
            sizes: '180x180',
            type: 'image/png'
          },
          {
            src: 'statics/icons/android-chrome-192x192.png',
            sizes: '192x192',
            type: 'image/png'
          },
          {
            src: 'statics/icons/mstile-150x150.png',
            sizes: '270x270',
            type: 'image/png'
          },
          /* {
            src: 'statics/icons/icon-256x256.png',
            sizes: '256x256',
            type: 'image/png'
          },
          {
            src: 'statics/icons/icon-384x384.png',
            sizes: '384x384',
            type: 'image/png'
          }, */
          {
            src: 'statics/icons/android-chrome-512x512.png',
            sizes: '512x512',
            type: 'image/png'
          }
        ]
      }
    },
    cordova: {
      // package.json
      // id: 'org.cordova.quasar.app',
      // description: 'Best PWA App in town!',
      // version: '0.1'
    },
    electron: {
      extendWebpack (cfg) {
        // do something with cfg
      },
      packager: {
        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',
        // Window only
        // win32metadata: { ... }
      }
    },

    // leave this here for Quasar CLI
    starterKit: '1.0.2'
  }
}
/*
alias: {
quasar: appPaths.resolve.app(`node_modules/quasar-framework/dist/quasar.${cfg.ctx.themeName}.esm.js`),
src: appPaths.srcDir,
components: appPaths.resolve.src(`components`),
layouts: appPaths.resolve.src(`layouts`),
pages: appPaths.resolve.src(`pages`),
assets: appPaths.resolve.src(`assets`),
variables: appPaths.resolve.app(`.quasar/variables.styl`),

// CLI using these ones:
'quasar-app-styl': appPaths.resolve.app(`.quasar/app.styl`),
'quasar-app-variables': appPaths.resolve.src(`css/themes/variables.${cfg.ctx.themeName}.styl`),
'quasar-styl': appPaths.resolve.app(`node_modules/quasar-framework/dist/quasar.${cfg.ctx.themeName}.styl`)
}
*/
